//
//  InterfaceController.swift
//  ActivityHR WatchKit Extension
//
//  Created by Admin on 4/25/16.
//  Copyright © 2016 Ubicomp Lab. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity


class InterfaceController: WKInterfaceController , HKWorkoutSessionDelegate, WCSessionDelegate {

    @IBOutlet weak var heartRateLabel: WKInterfaceLabel!
    @IBOutlet weak var activityLabel: WKInterfaceLabel!
    
    
    var counter = 0
    var message = ""
    var currentDataActivity=""
    var currentDeviceFit=""
    
    var session : WCSession!
    
    let healthStore = HKHealthStore()
    
    // flag to indicate whether the workout is activated or not
    var workoutActive = false
    
    // define the activity type and location
    var workoutSession : HKWorkoutSession?
    let heartRateUnit = HKUnit(fromString: "count/min")
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    
    
    var queryHK : HKQuery!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        // Activate the session on both sides to enable communication.        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        guard HKHealthStore.isHealthDataAvailable() == true else{
            heartRateLabel.setText("Not available")
            return
        }
        
        guard let quantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate) else{
            heartRateLabel.setText("Not allowed")
            return
        }
        
        let dataTypes = Set(arrayLiteral: quantityType)
        healthStore.requestAuthorizationToShareTypes(nil, readTypes: dataTypes) { (success, error) -> Void in
            if success == false{
                self.heartRateLabel.setText("Not Allowed")
            }
        }
        
    }
    
    @IBAction func getHeartRate() {
//        heartRateLabel.setText("111 BPM")
//        activityLabel.setText("Sitting")
        
        launchVoiceDictationMenu();
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func workoutSession(workoutSession: HKWorkoutSession, didChangeToState toState: HKWorkoutSessionState, fromState: HKWorkoutSessionState, date: NSDate) {
        switch toState {
        case .Running:
            workoutDidStart(date)
        case .Ended:
            workoutDidEnd(date)
        default:
            print("Unexpected state \(toState)")
        }
    }
    
    func workoutSession(workoutSession: HKWorkoutSession, didFailWithError error: NSError) {
        // Do nothing for now
        NSLog("Workout error: \(error.userInfo)")
    }
    
    func workoutDidStart(date : NSDate) {
        if let query = createHeartRateStreamingQuery(date) {
            queryHK = query
            healthStore.executeQuery(query)
        } else {
            heartRateLabel.setText("cannot start")
        }
    }
    
    @IBAction func textDictationTapped() {
        
        NSLog("dictation tapped");
        launchVoiceDictationMenu();
    }
    
    func launchVoiceDictationMenu(){
        self.presentTextInputControllerWithSuggestions(["Walk","Sit","Stand","Run","Stair"], allowedInputMode: WKTextInputMode.Plain,
                                                       completion:{(results) -> Void in
                                                        let aResult = results?[0] as? String
                                                        print(aResult);
                                                        NSLog("================== ACTIVITY ===============");
                                                        NSLog(aResult!);
                                                        
                                                        self.currentDataActivity = aResult!;
                                                        
                                                        self.launchVoiceDictationMenuForWatchState();
        })
    }
    
    func launchVoiceDictationMenuForWatchState(){
        self.presentTextInputControllerWithSuggestions(["Loose","Fit","Tight"], allowedInputMode: WKTextInputMode.Plain,
                                                       completion:{(results) -> Void in
                                                        let aResult = results?[0] as? String
                                                        print(aResult);
                                                        NSLog("================== ACTIVITY ===============");
                                                        NSLog(aResult!);
                                                        
                                                        self.currentDeviceFit = aResult!;
                                                        
                                                        
                                                        
                                                        if (self.workoutActive) {
                                                            //finish the current workout
                                                            self.workoutActive = false
                                                            //self.startStopButton.setTitle("Start")
                                                            if let workout = self.workoutSession {
                                                                self.healthStore.endWorkoutSession(workout)
                                                            }
                                                        } else {
                                                            //start a new workout
                                                            NSLog("Start workout")
                                                            self.message = ""
                                                            self.activityLabel.setText("Getting HR...")
                                                            self.workoutActive = true
                                                            //self.startStopButton.setTitle("Stop")
                                                            self.startWorkout()
                                                        }
        })
    }
    
    
    func workoutDidEnd(date : NSDate) {
        if let query = createHeartRateStreamingQuery(date) {
            healthStore.stopQuery(query)
            NSLog("Stop HR Query")
//            heartRateLabel.setText("--- BPM")
        } else {
            heartRateLabel.setText("cannot stop")
        }
    }
    
    func startWorkout() {
        self.workoutSession = HKWorkoutSession(activityType: HKWorkoutActivityType.CrossTraining, locationType: HKWorkoutSessionLocationType.Indoor)
        self.workoutSession?.delegate = self
        healthStore.startWorkoutSession(self.workoutSession!)
    }

    
    
    func createHeartRateStreamingQuery(workoutStartDate: NSDate) -> HKQuery? {
        // adding predicate will not work
        // let predicate = HKQuery.predicateForSamplesWithStartDate(workoutStartDate, endDate: nil, options: HKQueryOptions.None)
        
        guard let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate) else { return nil }
        
        let heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: nil, anchor: anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            guard let newAnchor = newAnchor else {return}
            self.anchor = newAnchor
            self.updateHeartRate(sampleObjects)
        }
        
        heartRateQuery.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
            self.anchor = newAnchor!
            self.updateHeartRate(samples)
        }
        return heartRateQuery
    }
    
    
    func updateHeartRate(samples: [HKSample]?) {
        guard let heartRateSamples = samples as? [HKQuantitySample] else {return}
        
        dispatch_async(dispatch_get_main_queue()) {
            guard let sample = heartRateSamples.first else{return}
            let value = sample.quantity.doubleValueForUnit(self.heartRateUnit)
            self.heartRateLabel.setText(String(UInt16(value)))
            
            
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "yyyyMMddHHmmss"
            
            let dateString = dayTimePeriodFormatter.stringFromDate(NSDate());
            //dateString now contains the string "Sunday, 7 AM".

            
            
            
            self.message = self.message + self.currentDeviceFit + "," + self.currentDataActivity + "," + dateString + "," + value.description + "\n"
            
            // retrieve source from sample
//            let name = sample.sourceRevision.source.name
//            self.updateDeviceName(name)
//            self.animateHeart()
        }

        self.counter = self.counter + 1
        self.activityLabel.setText("Count: " + self.counter.description)        
        
        NSLog("Count: %d", self.counter)
        
        if(self.counter > 3){
            if let workout = self.workoutSession {
                self.healthStore.endWorkoutSession(workout)
                self.healthStore.stopQuery(queryHK)
                self.sendHR(message)
                self.activityLabel.setText("Done!")
            }
            self.workoutActive = false
            self.counter = 0;
            self.currentDataActivity="";
            self.currentDeviceFit="";
        }
    }
    
    
    
    // =========================================================================
    // MARK: - WCSessionDelegate
    
//    func sessionWatchStateDidChange(session: WCSession) {
//        print(#function)
//        print(session)
//        print("reachable:\(session.reachable)")
//    }
//    
//    // Received message from iPhone
//    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
//        print(#function)
//        guard message["request"] as? String == "showAlert" else {return}
//        
//        let defaultAction = WKAlertAction(
//            title: "OK",
//            style: WKAlertActionStyle.Default) { () -> Void in
//        }
//        let actions = [defaultAction]
//        
//        presentAlertControllerWithTitle(
//            "Message Received",
//            message: "",
//            preferredStyle: WKAlertControllerStyle.Alert,
//            actions: actions)
//    }
    
    
    // =========================================================================
    // MARK: - Method to send HR
    
    func sendHR(message: String) {
        let requestValues = ["data" : message]
        let session = WCSession.defaultSession()
        
        session.sendMessage(requestValues, replyHandler: { reply in
            //self.activityLabel.setText(reply["data"] as? String)
            }, errorHandler: { error in
                print("error: \(error)")
        })
        
        NSLog("Data sent...")
    }


}
